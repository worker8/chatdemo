## Overview

This is a one page demo application to implement a chat activity in Android. 

### Features
- seed data from json file to populate db (only on first launch)
- showing messages 20 at a time with lazy load more pagination
- attachments are showed below each messages
- user id == 1 is assumed to me the 'session user'
- messages from others are aligned to the left, and messages from the session user (myself) are aligned to the right
- **long press** on message or attachment to trigger deletion action
- default animation is available when deletion happens
- `minSdkVersion` supported is `21`

## Dependencies

Below is the libraries chosen and why they are selected.

- ViewModel: this is for implementing the MVVM architecture
- Moshi: moshi is used to convert json into kotlin objects, it works better with Kotlin than gson
- RxJava, RxBinding, RxAndroid: the Rx stack changes the app development paradigm to reactive style, besides, threads usages are much easier.
- Room: this is used as the persistence layer to store messages deserialized from the seed json file. It works well with RxJava.
- Glide: this is for image loading. Picasso has lesser features and smaller in size. Glide has more features and larger. Chat application might need to support Gif in the future, Glide can be future proof for this.
- Stetho: this is used to debug the db. It can work to show network calls if Retrofit or Fuel is used.
- [worker8utils](https://github.com/worker8/worker8utils/): this a library that includes many commonly used functions, such as kotlin DSL for making simple dialogs, bottom detection for RecyclerView, etc

`buildSrc` folder is used to make dependency and versioning management easier.

## Screenshots

1. [Screenshot](https://user-images.githubusercontent.com/1988156/53695990-b7bebc80-3e05-11e9-8b3c-666d8c71dbd4.png)
2. [Gif for deleting message/attachments](https://user-images.githubusercontent.com/1988156/53696088-e8532600-3e06-11e9-9719-13058384a833.gif)

## Architecture explanations

![image](https://user-images.githubusercontent.com/1988156/53696273-acb95b80-3e08-11e9-8d98-70b743ab11aa.png)

1. ViewInput - events that are triggered by the UI, for example: event when bottom is reached, or event when message confirmation is clicked on deletion dialog
2. ViewAction - actions that don't need to be stored inside `ViewModel`, for example: showing progress bar. Because during a screen rotation, transient states like this don't need to persist
3. ScreenState - this is similar to `LiveData` from AAC, but since RxJava is used extensively, the `BehaviourSubject` can work the same way
4. Repo - this is the interface to work with the data, it provides methods to insert/delete stuff from the db, access shared preference, etc

## Todo
- Errors handling - no errors handling are implemented
- Lint errors - these are not checked yet, e.g. hardcoded strings should all be extracted into `strings.xml`
- Schema - this should be committed
- Tests - no tests are implemented, unit tests can be implemented the same way as [this example](https://github.com/worker8/Pixels/blob/master/app/src/test/java/beepbeep/pixelsforredditx/home/HomeViewModelTest.kt)
- Styles - styles are not checked, implementing `AppThemeDark` will ensure that all the colros are not hardcoded

## Coding styles
`.editorconfig` is used in this project to make sure that the spacing and indentations are standardized, the `editorconfig` is obtained from [ktlint project](https://github.com/shyiko/ktlint/blob/master/.editorconfig). 


