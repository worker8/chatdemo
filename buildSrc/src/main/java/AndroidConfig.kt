class AndroidConfig {
    val sdkVersion = 28
    val minSdkVersion = 21
    val targetSdkVersion = 28
    val versionCode = 1
    val versionName = "0.0.1"
}
