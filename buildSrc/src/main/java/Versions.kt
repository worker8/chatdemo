object Versions {
    val Tools = Tool()
    val AndroidConfigs = AndroidConfig()

    /* common utils */
    val worker8Utils = "0.0.2" // own repo

    /* image loading */
    val glide = "4.8.0"
    val glideTransformation = "4.0.1"

    /* Android Support */
    val appCompat = "1.0.2"

    /* Android UI */
    val constraintLayout = "1.1.3"
    val material = "1.1.0-alpha03"

    /* json handling */
    val moshi = "1.8.0"

    /* Android Arch Component */
    val archLifecycle = "2.1.0-alpha02"

    /* Reactive stack */
    val rxJava = "2.1.13"
    val rxAndroid = "2.0.2"
    val rxBinding = "3.0.0-alpha2"

    /* Persistence: Room */
    val room = "2.1.0-alpha04"

    /* Debug */
    val stetho = "1.5.0"

    /* Test */
    val junit = "4.12"
    val testRunner = "1.1.0-alpha4"
    val espressoCore = "3.1.0-alpha4"
}

class Tool {
    val kotlin = "1.3.20"
    val buildGradle = "3.3.0"
}
