package com.github.chatdemo.utils

import android.content.Context
import androidx.annotation.AnyRes
import java.io.IOException

/* reference: https://readyandroid.wordpress.com/read-json-or-txt-file-from-assets-folder/ */
fun Context.readJson(@AnyRes rawResId: Int): String {
    var json = ""
    try {
        val inputStream = resources.openRawResource(rawResId)
        val size = inputStream.available()
        val buffer = ByteArray(size)
        inputStream.read(buffer)
        inputStream.close()
        json = String(buffer)
    } catch (ex: IOException) {
        ex.printStackTrace()
    }
    return json
}

