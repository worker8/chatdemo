package com.github.chatdemo

import android.app.Application
import androidx.room.Room
import com.facebook.stetho.Stetho
import com.github.chatdemo.model.db.ChatDatabase
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory

class ChatApplication : Application() {
    val moshi: Moshi by lazy {
        Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
    }

    val chatDatabase: ChatDatabase by lazy {
        Room.databaseBuilder(this, ChatDatabase::class.java, "ChatDatabase").build()
    }

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(this)
        }
    }
}
