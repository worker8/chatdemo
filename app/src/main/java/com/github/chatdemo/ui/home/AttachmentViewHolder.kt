package com.github.chatdemo.ui.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.github.chatdemo.R
import kotlinx.android.synthetic.main.row_attachment.view.*

class AttachmentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind(attachmentRowData: HomeAdapter.ChatRowType.Attachment,
             onAttachmentLongClicked: (Pair<Long, String>) -> Unit) {
        val attachment = attachmentRowData.attachment
        itemView.apply {
            Glide.with(context)
                .load(attachment.url)
                .apply(RequestOptions.centerCropTransform())
                .into(attachmentImageView)
            attachmentText.text = attachment.title
            setOnLongClickListener {
                onAttachmentLongClicked.invoke(attachmentRowData.messageId to attachment.id)
                true
            }
        }
    }

    companion object {
        fun create(parent: ViewGroup) =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.row_attachment, parent, false)
                .let { AttachmentViewHolder(it) }

    }
}
