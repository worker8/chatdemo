package com.github.chatdemo.ui.home

import io.reactivex.Observable

class HomeContract {
    interface ViewInput {
        val onBottomReached: Observable<Unit>
        val onMessageDeleteConfirmedClicked: Observable<Long>
        val onAttachmentDeleteConfirmedClicked: Observable<Pair<Long, String>>
    }

    interface ViewAction {
        fun setLoadMoreProgressBar(visibility: Boolean)
        fun setEmptyProgressBar(visibility: Boolean)
    }

    data class ScreenState(
        val messages: Set<HomeAdapter.ChatRowType> = setOf()
    )
}
