package com.github.chatdemo.ui.home

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.github.chatdemo.model.TwistMessageWithTwistUser
import com.github.chatdemo.model.db.TwistAttachment
import io.reactivex.subjects.PublishSubject


class HomeAdapter : ListAdapter<HomeAdapter.ChatRowType, RecyclerView.ViewHolder>(TwistComparator) {
    private val onMessageClickedSubject: PublishSubject<Long> = PublishSubject.create()
    val onMessageClickedObservable = onMessageClickedSubject.hide()

    private val onAttachmentClickedSubject: PublishSubject<Pair<Long, String>> = PublishSubject.create()
    val onAttachmentClickedObservable = onAttachmentClickedSubject.hide()

    init {
        hasStableIds()
    }

    override fun getItemViewType(position: Int) = getItem(position).typeNumber

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            0 -> {
                ChatViewHolder.create(parent)
            }
            1 -> {
                ChatSelfViewHolder.create(parent)
            }
            else -> {
                AttachmentViewHolder.create(parent)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val chatRow = getItem(position)
        when (chatRow.typeNumber) {
            0 -> {
                val messageWithUser: TwistMessageWithTwistUser = (chatRow as ChatRowType.Message).messageWithUser
                (holder as ChatViewHolder).bind(
                    messageWithUser = messageWithUser,
                    onMessageLongClicked = { onMessageClickedSubject.onNext(messageWithUser.twistMessage.id) })
            }
            1 -> {
                val messageWithUser: TwistMessageWithTwistUser = (chatRow as ChatRowType.MessageOwn).messageWithUser
                (holder as ChatSelfViewHolder).bind(
                    messageWithUser = messageWithUser,
                    onMessageLongClicked = { onMessageClickedSubject.onNext(messageWithUser.twistMessage.id) }
                )
            }
            else -> {
                val attachmentRowData = (chatRow as ChatRowType.Attachment)
                (holder as AttachmentViewHolder).bind(
                    attachmentRowData = attachmentRowData,
                    onAttachmentLongClicked = { onAttachmentClickedSubject.onNext(it) })
            }
        }
    }

    sealed class ChatRowType(open val id: Long, val typeNumber: Int) {
        data class Message(val messageWithUser: TwistMessageWithTwistUser) :
            ChatRowType(id = messageWithUser.twistMessage.id.toString().hashCode().toLong(), typeNumber = 0)

        data class MessageOwn(val messageWithUser: TwistMessageWithTwistUser) :
            ChatRowType(id = messageWithUser.twistMessage.id.toString().hashCode().toLong(), typeNumber = 1)

        data class Attachment(val messageId: Long, val attachment: TwistAttachment) :
            ChatRowType(id = attachment.id.hashCode().toLong(), typeNumber = 2)
    }

    companion object {
        val TwistComparator = object : DiffUtil.ItemCallback<ChatRowType>() {
            override fun areItemsTheSame(oldItem: ChatRowType, newItem: ChatRowType) =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: ChatRowType, newItem: ChatRowType) =
                oldItem == newItem
        }
    }
}
