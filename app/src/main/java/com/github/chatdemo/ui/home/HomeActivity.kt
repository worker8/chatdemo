package com.github.chatdemo.ui.home

import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.github.chatdemo.ChatApplication
import com.github.chatdemo.R
import com.github.chatdemo.utils.addTo
import com.github.worker8utilslib.common.extension.initBottomDetectListener
import com.github.worker8utilslib.common.extension.makeDialog
import com.github.worker8utilslib.common.extension.onBottomDetectedObservable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.activity_home.*
import java.util.concurrent.TimeUnit

class HomeActivity : AppCompatActivity() {
    private val twistApplication by lazy { application as ChatApplication }
    private val moshi by lazy { twistApplication.moshi }
    private val db by lazy { twistApplication.chatDatabase }
    private val onMessageDeleteConfirmedClickedSubject: PublishSubject<Long> = PublishSubject.create()
    private val onAttachmentDeleteConfirmedClickedSubject: PublishSubject<Pair<Long, String>> = PublishSubject.create()
    private val input = object : HomeContract.ViewInput {
        override val onAttachmentDeleteConfirmedClicked by lazy { onAttachmentDeleteConfirmedClickedSubject.hide() }
        override val onBottomReached by lazy { homeRecyclerView.onBottomDetectedObservable }
        override val onMessageDeleteConfirmedClicked by lazy { onMessageDeleteConfirmedClickedSubject.hide() }
    }
    private val viewAction = object : HomeContract.ViewAction {
        override fun setLoadMoreProgressBar(visibility: Boolean) {
            homeLoadMoreProgressBar.visibility = if (visibility) {
                View.VISIBLE
            } else {
                View.GONE
            }
        }

        override fun setEmptyProgressBar(visibility: Boolean) {
            homeEmptyProgressbar.visibility = if (visibility) {
                View.VISIBLE
            } else {
                View.GONE
            }
        }
    }
    private val repo by lazy {
        HomeRepo(
            context = this@HomeActivity,
            moshi = moshi,
            db = db
        )
    }
    private val dialogList =
        mutableListOf<AlertDialog>() // this is for calling .dismiss() from all dialog: https://stackoverflow.com/a/2850597/75579
    val homeAdapter = HomeAdapter()
    private val disposableBag = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setupViews()

        val viewModel =
            ViewModelProviders.of(this, HomeViewModelFactory(repo = repo, input = input, viewAction = viewAction))
                .get(HomeViewModel::class.java)

        lifecycle.addObserver(viewModel)
        viewModel.screenState
            .distinctUntilChanged()
            .debounce(300L, TimeUnit.MILLISECONDS)
            .observeOn(repo.getMainThread())
            .subscribe {
                homeAdapter.submitList(it.messages.toList())
            }
            .addTo(disposableBag)
    }

    private fun setupViews() {
        homeRecyclerView.apply {
            adapter = homeAdapter
            initBottomDetectListener()
        }

        homeAdapter.onMessageClickedObservable
            .observeOn(repo.getMainThread())
            .subscribe { messageId ->
                makeDialog {
                    messageTitle = "Confirmation"
                    // messageText = "Are you sure you want to delete message #${messageId}?" // uncomment for better debugging
                    messageText = "Are you sure you want to delete this message?"
                    positiveText = "OK"
                    positiveOnClickListener = { dialog ->
                        onMessageDeleteConfirmedClickedSubject.onNext(messageId)
                        dialog.dismiss()
                    }
                    negativeText = "Cancel"
                    negativeOnClickListener = { dialog ->
                        dialog.dismiss()
                    }
                }
                    .apply { show() }
                    .let { dialogList.add(it) }
            }
            .addTo(disposableBag)

        homeAdapter.onAttachmentClickedObservable
            .observeOn(repo.getMainThread())
            .subscribe { (messageId, attachmentId) ->
                makeDialog {
                    messageTitle = "Confirmation"
                    messageText = "Are you sure you want to delete this attachment?"
                    positiveText = "OK"
                    positiveOnClickListener = { dialog ->
                        onAttachmentDeleteConfirmedClickedSubject.onNext(messageId to attachmentId)
                        dialog.dismiss()
                    }
                    negativeText = "Cancel"
                    negativeOnClickListener = { dialog ->
                        dialog.dismiss()
                    }
                }
                    .apply { show() }
                    .let { dialogList.add(it) }
            }
            .addTo(disposableBag)

    }

    override fun onDestroy() {
        super.onDestroy()
        disposableBag.dispose()
        dialogList.forEach { it.dismiss() }
    }
}
