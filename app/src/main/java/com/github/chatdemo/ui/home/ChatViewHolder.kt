package com.github.chatdemo.ui.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.github.chatdemo.R
import com.github.chatdemo.model.TwistMessageWithTwistUser
import kotlinx.android.synthetic.main.row_chat.view.*

class ChatViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind(
        messageWithUser: TwistMessageWithTwistUser,
        onMessageLongClicked: () -> Unit
    ) {
        val twistMessage = messageWithUser.twistMessage
        val twistUser = messageWithUser.user[0]

        itemView.apply {
            messageText.text = twistMessage.content
            // messageUsername.text = "${twistMessage.id}: ${twistUser.name}"
            messageUsername.text = twistUser.name

            Glide.with(context)
                .load(twistUser.avatarId)
                .apply(RequestOptions.circleCropTransform())
                .into(messageAvatar)

            messageTextGroup.setOnLongClickListener {
                onMessageLongClicked.invoke()
                true
            }
        }
    }

    companion object {
        fun create(parent: ViewGroup) =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.row_chat, parent, false)
                .let { ChatViewHolder(it) }

    }
}
