package com.github.chatdemo.ui.home

import android.content.Context
import android.content.SharedPreferences
import com.github.chatdemo.R
import com.github.chatdemo.model.TwistResponse
import com.github.chatdemo.model.db.ChatDatabase
import com.github.chatdemo.utils.readJson
import com.github.worker8utilslib.common.defaultPrefs
import com.github.worker8utilslib.common.get
import com.github.worker8utilslib.common.save
import com.squareup.moshi.Moshi
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class HomeRepo(val context: Context, val moshi: Moshi, val db: ChatDatabase) {
    var page = 1
    val prefs: SharedPreferences = context.defaultPrefs()

    fun isFirstLaunch() =
        prefs.get(IsFirstLaunchKey, true)

    fun setFirstLaunchFalse() =
        prefs.save(IsFirstLaunchKey, false)

    fun seedData(): Observable<TwistResponse?> =
        Observable
            .fromCallable {
                val jsonString = context.readJson(R.raw.data)
                val jsonAdapter = moshi.adapter(TwistResponse::class.java)
                jsonAdapter.fromJson(jsonString)
            }
            .doOnNext {
                insertIntoDb(it)
            }

    fun getTwistMessageWithTwistUser() = db.twistMessageDao().getTwistMessageWithTwistUser()

    fun getNextMessages() =
        db.twistMessageDao()
            .getTwistMessageWithTwistUserByPage(pageSize = PageSize * page, offset = 0)
            .apply { page++ }

    private fun insertIntoDb(twistResponse: TwistResponse?) {
        twistResponse?.apply {
            db.insertMessagesAndUsers(messages, users)
        }
    }

    fun delete(messageId: Long) = db.twistMessageDao().deleteById(messageId)
    fun deleteAttachmentById(id: Long, attachmentId: String) =
        db.twistMessageDao().deleteAttachmentById(id, attachmentId)

    fun getBackgroundThread() = Schedulers.io()
    fun getMainThread() = AndroidSchedulers.mainThread()

    companion object {
        const val PageSize = 20
        const val IsFirstLaunchKey = "IsFirstLaunchKey"
    }
}
