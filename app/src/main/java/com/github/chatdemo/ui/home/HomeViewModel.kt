package com.github.chatdemo.ui.home

import androidx.lifecycle.*
import com.github.chatdemo.utils.addTo
import com.github.chatdemo.utils.realValue
import io.reactivex.BackpressureStrategy
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.BehaviorSubject

class HomeViewModel(
    val repo: HomeRepo,
    val input: HomeContract.ViewInput,
    val viewAction: HomeContract.ViewAction
) : ViewModel(), LifecycleObserver {
    private val disposableBag = CompositeDisposable()
    private val screenStateSubject = BehaviorSubject.createDefault<HomeContract.ScreenState>(HomeContract.ScreenState())
    private val currentScreenState get() = screenStateSubject.realValue
    var screenState = screenStateSubject.hide().observeOn(repo.getMainThread())

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreate() {
        if (repo.isFirstLaunch()) {
            viewAction.setEmptyProgressBar(true)
            repo.seedData()
                .subscribeOn(repo.getBackgroundThread())
                .observeOn(repo.getMainThread())
                .subscribe({
                    repo.setFirstLaunchFalse()
                    viewAction.setEmptyProgressBar(false)
                }, {
                    viewAction.setEmptyProgressBar(false)
                })
                .addTo(disposableBag)
        }

        Observable.merge(Observable.just(Unit), input.onBottomReached)
            .subscribeOn(repo.getBackgroundThread())
            .observeOn(repo.getMainThread())
            .doOnNext { viewAction.setLoadMoreProgressBar(true) }
            .observeOn(repo.getBackgroundThread())
            .toFlowable(BackpressureStrategy.DROP)
            .flatMap { repo.getNextMessages() }
            .map { messages ->
                val tempList: MutableList<HomeAdapter.ChatRowType> = mutableListOf()
                messages.forEach { message ->

                    message.twistMessage.attachments?.let { _attachments ->
                        if (_attachments.isNotEmpty()) {
                            _attachments.forEach { attachment ->
                                tempList.add(
                                    HomeAdapter.ChatRowType.Attachment(message.twistMessage.id, attachment)
                                )
                            }
                        }
                    }
                    if (message.user[0].isUserMyself()) {
                        tempList.add(HomeAdapter.ChatRowType.MessageOwn(message))
                    } else {
                        tempList.add(HomeAdapter.ChatRowType.Message(message))
                    }
                }
                tempList
            }
            // uncomment to see the pagination progress bar
            //.delay(1000, TimeUnit.MILLISECONDS)
            .observeOn(repo.getMainThread())
            .subscribe({
                viewAction.setLoadMoreProgressBar(false)
                dispatch(currentScreenState.copy(messages = it.toSet()))
            }, {
                viewAction.setLoadMoreProgressBar(false)
            })
            .addTo(disposableBag)

        input.onMessageDeleteConfirmedClicked
            .observeOn(repo.getBackgroundThread())
            .subscribe { messageId -> repo.delete(messageId) }
            .addTo(disposableBag)

        input.onAttachmentDeleteConfirmedClicked
            .observeOn(repo.getBackgroundThread())
            .subscribe { (messageId, attachmentId) ->
                repo.deleteAttachmentById(messageId, attachmentId)
            }
            .addTo(disposableBag)

    }

    private fun dispatch(screenState: HomeContract.ScreenState) {
        screenStateSubject.onNext(screenState)
    }

    override fun onCleared() {
        super.onCleared()
        disposableBag.dispose()
    }
}

@Suppress("UNCHECKED_CAST")
class HomeViewModelFactory(
    val repo: HomeRepo,
    val input: HomeContract.ViewInput,
    val viewAction: HomeContract.ViewAction
) :
    ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return HomeViewModel(repo, input, viewAction) as T
    }
}
