package com.github.chatdemo.ui.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.github.chatdemo.R
import com.github.chatdemo.model.TwistMessageWithTwistUser
import kotlinx.android.synthetic.main.row_chat.view.*

class ChatSelfViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind(
        messageWithUser: TwistMessageWithTwistUser,
        onMessageLongClicked: () -> Unit
    ) {
        val twistMessage = messageWithUser.twistMessage
        val twistUser = messageWithUser.user[0]

        itemView.apply {
            messageText.text = twistMessage.content
            messageUsername.text = context.getString(R.string.me)
            messageTextGroup.setOnLongClickListener {
                onMessageLongClicked.invoke()
                true
            }
        }
    }

    companion object {
        fun create(parent: ViewGroup) =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.row_chat_self, parent, false)
                .let { ChatSelfViewHolder(it) }

    }
}
