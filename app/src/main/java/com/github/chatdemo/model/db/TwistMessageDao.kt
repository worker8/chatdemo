package com.github.chatdemo.model.db

import android.util.Log
import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import com.github.chatdemo.model.TwistMessage
import com.github.chatdemo.model.TwistMessageWithTwistUser
import io.reactivex.Flowable

@Dao
interface TwistMessageDao : BaseDao<TwistMessage> {
    @Transaction
    @Query("SELECT * FROM TwistMessage ORDER BY id DESC")
    fun getTwistMessageWithTwistUser(): Flowable<List<TwistMessageWithTwistUser>>

    @Query("DELETE FROM twistmessage WHERE id = :messageId")
    fun deleteById(messageId: Long)

    @Transaction
    fun deleteAttachmentById(messageId: Long, attachmentId: String) {
        val foundMessages = findById(messageId)
        foundMessages?.also { _foundMessages ->
            if (_foundMessages.isNotEmpty()) {
                val foundMessage = _foundMessages[0]
                val mutableAttachments = foundMessage.attachments?.toMutableList()
                mutableAttachments?.removeAll { _twistAttachment ->
                    _twistAttachment.id == attachmentId
                }
                foundMessage.attachments = mutableAttachments
                insert(foundMessage)
            }
        }
    }

    @Query("SELECT * FROM twistmessage WHERE id = :id ")
    fun findById(id: Long): List<TwistMessage>?

    @Transaction
    @Query("SELECT * FROM TwistMessage ORDER BY id DESC LIMIT :pageSize OFFSET :offset ")
    fun getTwistMessageWithTwistUserByPage(
        pageSize: Int = 20,
        offset: Int = 0
    ): Flowable<List<TwistMessageWithTwistUser>>
}
