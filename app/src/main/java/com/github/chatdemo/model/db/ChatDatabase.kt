package com.github.chatdemo.model.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.Transaction
import androidx.room.TypeConverters
import com.github.chatdemo.model.TwistMessage
import com.github.chatdemo.model.TwistUser
import com.github.chatdemo.model.db.typeConverter.RoomTypeAdapter

//TODO: Remember to commit database schema at the end..

@Database(entities = [TwistMessage::class, TwistUser::class], version = 1)
@TypeConverters(RoomTypeAdapter::class)
abstract class ChatDatabase : RoomDatabase() {
    abstract fun twistMessageDao(): TwistMessageDao
    abstract fun twistUserDao(): TwistUserDao

    @Transaction
    fun insertMessagesAndUsers(messages: List<TwistMessage>, users: List<TwistUser>) {
        twistMessageDao().insert(messages)
        twistUserDao().insert(users)
    }
}
