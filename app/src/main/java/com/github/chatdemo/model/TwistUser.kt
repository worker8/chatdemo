package com.github.chatdemo.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class TwistUser(
    @PrimaryKey
    var id: Long,
    var avatarId: String,
    var name: String
) {
    fun isUserMyself() = id == 1L // this is an assumption by the spec
}
