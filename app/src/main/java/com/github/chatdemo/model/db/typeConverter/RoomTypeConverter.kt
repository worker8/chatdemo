package com.github.chatdemo.model.db.typeConverter

import androidx.room.TypeConverter
import com.github.chatdemo.model.db.TwistAttachment
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory

class RoomTypeAdapter {
    @TypeConverter
    fun fromTwistAttachment(twistAttachment: List<TwistAttachment>?): String {
        twistAttachment?.let {
            // TODO: extract moshi builder to a method, so that moshi creation is consistent
            val moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
            // reference: https://github.com/square/moshi/issues/78#issuecomment-140155920
            val twistAttachmentListType = Types.newParameterizedType(List::class.java, TwistAttachment::class.java)
            val adapter = moshi.adapter<List<TwistAttachment>>(twistAttachmentListType)
            return adapter.toJson(twistAttachment)
        }
        return ""
    }

    @TypeConverter
    fun toTwistAttachment(twistAttachmentString: String): List<TwistAttachment>? {
        if (twistAttachmentString.isBlank()) {
            return null
        }
        //TODO: use same moshi builder here too
        val moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
        val twistAttachmentListType = Types.newParameterizedType(List::class.java, TwistAttachment::class.java)
        val adapter = moshi.adapter<List<TwistAttachment>>(twistAttachmentListType)
        return adapter.fromJson(twistAttachmentString)
    }
}
