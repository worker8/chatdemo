package com.github.chatdemo.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.github.chatdemo.model.db.TwistAttachment

@Entity
data class TwistMessage(
    @PrimaryKey
    var id: Long,
    var content: String,
    var userId: Long,
    var attachments: List<TwistAttachment>? = null
) {

}
