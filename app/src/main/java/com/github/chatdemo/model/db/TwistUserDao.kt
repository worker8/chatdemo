package com.github.chatdemo.model.db

import androidx.room.Dao
import com.github.chatdemo.model.TwistUser

@Dao
interface TwistUserDao : BaseDao<TwistUser> {
}
