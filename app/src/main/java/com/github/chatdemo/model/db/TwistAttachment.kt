package com.github.chatdemo.model.db

data class TwistAttachment(
    var id: String,
    var thumbnailUrl: String,
    var title: String,
    var url: String
)
