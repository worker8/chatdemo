package com.github.chatdemo.model

import com.squareup.moshi.JsonClass

/* reading material: https://medium.com/@ZacSweers/exploring-moshis-kotlin-code-gen-dec09d72de5e */
@JsonClass(generateAdapter = true)
data class TwistResponse(
    var messages: List<TwistMessage>,
    var users: List<TwistUser>
)
