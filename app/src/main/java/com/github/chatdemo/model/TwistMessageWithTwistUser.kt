package com.github.chatdemo.model

import androidx.room.Embedded
import androidx.room.Relation

data class TwistMessageWithTwistUser(
    @Embedded
    val twistMessage: TwistMessage,
    @Relation(parentColumn = "userId", entityColumn = "id", entity = TwistUser::class)
    val user: List<TwistUser>
)
